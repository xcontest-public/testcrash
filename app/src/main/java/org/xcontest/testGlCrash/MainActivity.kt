package org.xcontest.testGlCrash

import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.*
import org.oscim.android.MapView
import org.oscim.core.BoundingBox
import org.oscim.core.GeoPoint
import org.oscim.core.MapPosition
import org.oscim.layers.GroupLayer
import org.oscim.layers.tile.vector.VectorTileLayer
import org.oscim.layers.tile.vector.labeling.LabelLayer
import org.oscim.map.Map
import org.oscim.theme.VtmThemes
import org.oscim.tiling.source.mapfile.MultiMapFileTileSource
import org.xcontest.testGlCrash.databinding.ActivityMainBinding
import java.io.File
import java.io.RandomAccessFile
import java.nio.ByteBuffer

class MainActivity : AppCompatActivity(), CoroutineScope by MainScope() {

    private val mTileSources = MultiMapFileTileSource()
    private lateinit var binding : ActivityMainBinding

    private var running = false;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.click.setOnClickListener {
            removeMap()
            addMap()
        }

        binding.clickFile1.setOnClickListener {
            if (!running) {
                running = true;
                launch(Dispatchers.IO) {
                    runFileTest1()
                    running = false;
                }
            }
        }

        binding.clickFile2.setOnClickListener {
            if (!running) {
                running = true;
                launch(Dispatchers.IO) {
                    runFileTest2()
                    running = false;
                }
            }
        }

    }

    private suspend fun setText(text: String) {
        Log.e("job", text)
        withContext(Dispatchers.Main) {
            binding.textResults.text = text
        }
    }

    val buffer : ByteBuffer
    val buffer2 : ByteBuffer
    val sigbuffer : ByteBuffer

    init {
        buffer = ByteBuffer.allocate(4300)
        (0..4096).forEach { buffer.put(65)} // 'A'
        (0..30).forEach { buffer.put(66)} // 'B'

        buffer2 = ByteBuffer.allocate(4300)
        (0..4110).forEach { buffer2.put(67) } // 'C'
        sigbuffer = ByteBuffer.allocate(200)
        (0..100).forEach { sigbuffer.put(83) } // 'S'
    }

    private fun writeFileStart(fname: File) {
        val f = RandomAccessFile(fname, "rw")
        f.seek(0)
        f.write(buffer.array(), 0, buffer.position())
        f.write(sigbuffer.array(), 0, sigbuffer.position())
        f.getFD().sync();
        f.close()
    }

    private fun writeFileCont(fname: File) {
        val f = RandomAccessFile(fname, "rw")
        f.seek(buffer.position().toLong())
        f.write(buffer2.array(), 0, buffer2.position())
        f.write(sigbuffer.array(), 0, sigbuffer.position())
        f.getFD().sync();
        f.close()
    }

    private fun checkFile(fname: File) : Boolean {
        val f = RandomAccessFile(fname, "rw")
        f.seek(4100)
        val b = f.readByte()
        f.close()
        return (b == 66.toByte())
    }

    private fun getFileName(n: Int) : File {
        val base = getExternalFilesDir("filetest")
        val f = File(base, "testfile_${n.toString().padStart(4, '0')}.txt")
        if (f.exists())
            f.delete()
        return f
    }

    private suspend fun checkResults(files: List<File>) {
        val results = files.map { checkFile(it) }
        if (results.all { it }) {
            setText("Test finished OK.")
        } else {
            val bad = results.mapIndexed { i, v -> Pair(files[i], v)}
                    .filter { (_, v) -> v }
                    .map { (f, _) -> f.name }
                    .joinToString(", ")
            setText("Errors: ${bad}")
        }
    }

    private suspend fun runFileTest1() {
        setText("Initializing test 1");
        val files = (0..1000).map { getFileName(it) }
        setText("Writing part 1");
        files.forEach { writeFileStart(it) }
        setText("Writing part 2");
        files.forEach { writeFileCont(it) }
        checkResults(files)
    }

    private suspend fun runFileTest2() {
        setText("Initializing test 2");
        val files = (0..1000).map { getFileName(it) }
        setText("Writing both parts");
        files.forEach {
            writeFileStart(it)
            writeFileCont(it)
        }
        checkResults(files)
    }

    // Map crash
    var mMapView : MapView? = null
    var mMap : Map? = null

    private fun removeMap() {
        val mapGroup : ViewGroup = binding.mapgroup
        mapGroup.removeAllViews()
        mMapView?.let {
            it.onDestroy()
            mMapView = null
            mMap = null
        }
    }


    private fun addMap() {
        mMapView = MapView(this)
        mMap = mMapView!!.map()
        val l: VectorTileLayer = mMap!!.setBaseMap(mTileSources)

        val groupLayer = GroupLayer(mMap)
        groupLayer.layers.add(LabelLayer(mMap, l))
        mMap!!.layers().add(groupLayer)

        mMap!!.setTheme(VtmThemes.NEWTRON)

        // add mapview
        val mapGroup : ViewGroup = findViewById(R.id.mapgroup)
        mapGroup.addView(mMapView)
        // avoid Android BUG with fullscreen being onTop, it suffices to add an empty view
        mapGroup.addView(View(this))

        val mPos = MapPosition()
        mPos.setPosition(GeoPoint(50.0, 14.0))
        val bbox = BoundingBox(50.0, 13.0, 52.0, 14.0)
        mPos.setByBoundingBox(bbox, 500, 500)
        mMap!!.setMapPosition(mPos)
    }
}
